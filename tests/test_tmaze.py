#!/usr/bin/env python
#
# File: test_tmaze.py
#
# Created: Wednesday, December  2 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#
import click

import matplotlib.pyplot as plt
import seaborn as sns

import multiprocessing as mp

from environment.tmaze import *
from learn.qlearn import *
from learn.advantage import *
from model.discrete import *
from model.lstm import *
from model.gru import *
from model.mut1 import *
from model.mut2 import *
from model.mut3 import *

EPISODES = 50000
MAX_TIMESTEPS = 500
DISOUNT_FACTOR = 0.98
BATCH_SIZE = 10

HISTORY_LENGTH = 10
HIDDEN_NEURONS = 100


def explore(make_model, strat, **kwargs):
  for i in range(5, 75, 5):
    env = TMaze(i,1)
    el = []
    el.append(QLearning(env.num_actions(), 0.2, DISOUNT_FACTOR, strat, kwargs))
    el.append(BatchQLearning(env.num_actions(), 0.6, DISOUNT_FACTOR, strat, kwargs))
    el.append(AdvantageLearning(env.num_actions(), 0.6, DISOUNT_FACTOR, strat, kwargs))
    el.append(BatchAdvantageLearning(env.num_actions(), 0.6, DISOUNT_FACTOR, strat, kwargs))
    model = make_model(env.num_actions())
    for l in el:
      episodes = env.run(model, l, EPISODES, MAX_TIMESTEPS, BATCH_SIZE)

      plt.figure()
      plt.plot([e.cum_reward for e in episodes], '.')
      plt.xlabel('Iteration')
      plt.ylabel('Cumulative Reward')
      plt.savefig('rewards_{0}_{1}.pdf'.format(l.__name__, strat))

      env.plot_model(model, name='model_{0}_{1}'.format(l.__name__, strat))
  
@click.command()
@click.option('--history', default=10, help='Length of History')
@click.option('--hiddenn', default=100, help='Number of hidden neurons')
def main(history, hiddenn):
  discretemodel = lambda n: DiscreteModel(n)
  lstmmodel = lambda n: LSTMModel(n, HISTORY_LENGTH, HIDDEN_NEURONS)
  grumodel = lambda n: GRUModel(n, HISTORY_LENGTH, HIDDEN_NEURONS)
  mut1model = lambda n: MUT1Model(n, HISTORY_LENGTH, HIDDEN_NEURONS)
  mut2model = lambda n: MUT2Model(n, HISTORY_LENGTH, HIDDEN_NEURONS)
  mut3model = lambda n: MUT3Model(n, HISTORY_LENGTH, HIDDEN_NEURONS)

  tasks = []
  for m in [discretemodel, lstmmodel, grumodel, mut1model, mut2model, mut3model]:
    tasks.append(mp.Process(explore, args=(m, 'egreedy'), kwargs={'epsilon':0.9}))
    tasks.append(mp.Process(explore, args=(m, 'softmax'), kwargs={'temperature':0.5}))
  
  for t in tasks:
    t.start()
  for t in taskss:
    t.join()
  
main()  
