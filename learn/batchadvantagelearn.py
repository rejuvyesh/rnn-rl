#
# File: batchadvantagelearn.py
#
# Created: Tuesday, December  1 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#
from .abstract import Learning
from explore import explore # FIXME


class BatchAdvantageLearning(object):
  """ Batch Advantage Learning Strategy
  """

  def __init__(self, num_actions, alpha, gamma, kappa, exploration=None, **kwargs):
    """ Constructor
    
    Arguments:
    - `num_actions`:
    - `alpha`:
    - `gamma`:
    - `kappa`:
    - `exploration`:
    - `**kwargs`:
    """
    super(BatchAdvantageLearning, self).__init__(num_actions)
    self._alpha = alpha
    self._gamma = gamma
    self._kappa = kappa
    self._exploration = exploration
    self._kwargs = kwargs
    
  @explore
  def actions(self, episode):
    return episode.values[-1], 0.0, self.num_actions
  
  def end_episode(self, episode):
    for t in range(len(episode.states)-1, 0, -1):
      prev_action = episode.actions[t-1]
      prev_reward = episode.rewards[t-1]
      prev_values = episode.values[t-1]
      next_values = episode.values[t]
      
      advantage = prev_values[prev_action]
      value = max(prev_values)
      
      error = prev_reward + (self._gamma * max(next_values) - value)/self._kappa - advantage
      
      prev_values[prev_action] = advantage + self._alpha * error
      
      
