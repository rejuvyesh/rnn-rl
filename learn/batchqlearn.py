#
# File: batchqlearn.py
#
# Created: Tuesday, December  1 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#

from .abstract import Learning
from explore import explore # FIXME

class BatchQLearning(object):
  """ Batch Q learning Strategy
  """

  def __init__(self, num_actions, alpha, gamma, exploration=None, **kwargs):
    """ Constructor
    
    Arguments:
    - `num_actions`:
    - `alpha`:
    - `gamma`:
    - `exploration`:
    - `**kwargs`:
    """
    super(BatchQLearning, self).__init__(num_actions)
    self._alpha = alpha
    self._gamma = gamma
    self._exploration = exploration
    self._kwargs = kwargs
    
    @explore
    def actions(self, episode):
     """ Use with eGreedy or Softmax
     """ 
     return episode.values[-1], 0.0, self.num_actions
   
    def end_episode(self, epsiode):
      for t in range(len(episode.states)-1, 0, -1):
        prev_action = episode.actions[t-1]
        prev_reward = episode.rewards[t-1]
        prev_values = episode.values[t-1]
        next_values = epsiode.values[t]
        
        q = prev_values[prev_action]
        error = prev_reward + self._gamma * max(next_values) - q
        
        prev_values[prev_action] = q + self._alpha * error
       
       
