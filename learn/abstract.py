#
# File: abstract.py
#
# Created: Monday, November 30 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#

class Learning(object):
  """ Abstract class for reinforcement learning strategies
  """

  def __init__(self, num_actions):
    """ Constructor
    
    Arguments:
    - `num_actions`:
    """
    self.num_actions = num_actions
    
  def actions(self, episode):
    raise NotImplementedError()

  def end_episode(self, episode):
    self.actions(episode)
