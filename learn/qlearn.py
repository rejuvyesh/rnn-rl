#
# File: qlearn.py
#
# Created: Monday, November 30 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#

from .abstract import Learning
from explore import explore # FIXME

class QLearning(Learning):
  """ Q-learning strategy
  """

  def __init__(self, num_actions, alpha, gamma, exploration=None, **kwargs):
    """ Constructor
    
    Arguments:
    - `num_actions`:
    - `alpha`:
    - `gamma`:
    - `exploration`:
    """
    super(QLearning, self).__init__(num_actions)
    self._alpha = alpha
    self._gamma = gamma
    self._exploration = exploration
    self._kwargs = kwargs
    
  @explore
  def actions(self, episode):
    if len(episode.actions) > 0:
      last_action = episode.actions[-1]
      last_reward = episode.rewards[-1]
      
      q = episode.values[-2][last_action]
      error = last_reward + self._gamma * max(episode.values[-1]) - q
      
      episode.values[-2][last_action] = q + self._alpha * error
    else:
      error = 0.0
      
    return episode.values[-1], error, self.num_actions

  def __str__(self):
    return "alpha={0}, gamma={1}, explore={2}".format(self._alpha, self._gamma, self._exploration)
