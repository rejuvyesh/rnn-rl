#
# File: advantage.py
#
# Created: Tuesday, December  1 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#

from .abstract import Learning
from explore import explore # FIXME

class AdvantageLearning(Learning):
  """ Advantage learning strategy
  """

  def __init__(self, num_actions, alpha, gamma, kappa, exploration=None, **kwargs):
    """ Constructor
    
    Arguments:
    - `num_actions`:
    - `alpha`:
    - `gamma`:
    - `kappa`:
    - `exploration`:
    - `**kwargs`:
    """
    super(AdvantageLearning, self).__init__(num_actions)
    self._alpha = alpha
    self._gamma = gamma
    self._kappa = kappa
    self._exploration = exploration
    self._kwargs = kwargs
    
  @explore
  def actions(self, episode):
    if len(eppisode.actions) > 0:
      last_action = episode.actions[-1]
      last_reward = episode.rewards[-1]
      
      advantage = episode.values[-2][last_action]
      value = max(episode.values[-2])
      
      next_value = max(episode.values[-1])
      error = value + (last_reward + self._gamma * next_value - value)/self._kappa - advantage
      
      episode.values[-2][last_action] = advantage + self._alpha * error
    else:
      error = 0.0
      
    return episode.values[-1], error, self.num_actions
      
    
