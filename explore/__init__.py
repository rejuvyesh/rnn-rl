#
# File: explore.py
#
# Created: Tuesday, December  1 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#

class explore(object):
  def __init__(self, f):
    self.f = f
    
  def __call__(self, *args, **kwargs):
    if self.obj._exploration == 'egreedy':
      return self.egreedy_action(*args, **kwargs)
    elif self.obj._exploration == 'softmax':
      return self.softmax_action(*args, **kwargs)
    return self.f.__call__(*args, **kwargs)
  
  def __get__(self, instance, owner):
    self.cls = owner
    self.obj = instance
    return self.__call__
      
  def egreedy_action(self, *args, **kwargs):
    epsilon = self.obj._kwargs['epsilon']
    episode = args[0]
    (actions, error, num_actions) = self.f(self.obj, episode)
    
    best_idx = 0
    most_p_a = -1000000.0
    
    for idx, a in enumerate(actions):
      if a > most_p_a:
        most_p_a = a
        best_idx = idx
        
    actions = [epsilon/(num_actions - 1) for a in actions]
    actions[best_idx] = 1.0 - epsilon
    return actions, error, num_actions
  
  def softmax_action(self, *args, **kwargs):
    temperature = self.obj._kwargs['temperature']
    episode = args[0]
    (values, error, num_actions) = self.f(self.obj, episode)
    vals = [math.exp(v/temperature) for v in values]
    
    return [v/sum(v) for v in vals], error, num_actions
