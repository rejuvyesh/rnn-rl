#
# File: abstract.py
#
# Created: Monday, November 30 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#

class Model(object):
  """ Model associating values
  """

  def __init__(self, num_actions):
    """ Constructor
    
    Arguments:
    - `num_actions`:
    """
    self._num_actions = num_actions
    
  def learn(self, episodes):
    raise NotImplementedError()
  
  def values(self, episode):
    raise NotImplementedError()
  
  def plot_values(self, episode):
    return self.values(episode)
  
