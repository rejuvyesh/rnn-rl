#
# File: lstm.py
#
# Created: Tuesday, December  1 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#

try:
  from keras.models import Sequential
  from keras.layers.core import Dense
  from keras.layers.recurrent import LSTM
except ImportError:
  print('Keras NOT INSTALLED')
  
from .kerashistory import *

class LSTMModel(KerasHistory):
  """ Use LSTM cells to associate value to sequence of observations
  """

  def __init__(self, num_actions, history_length, hidden_neurons):
    """ Constructor
    
    Arguments:
    - `num_actions`:
    - `history_length`:
    - `hidden_neurons`:
    """
    super(LSTMModel, self).__init__(num_actions, history_length, hidden_neurons)
    
  def create_keras_model(self, state_size):
    model = Sequential()
    model.add(LSTM(input_shape=(self._history_length, state_size), 
                   # batch_input_shape=(10, self._history_length, state_size),
                   output_dim=self._hidden_neurons, 
                   activation='tanh', 
                   inner_activation='linear'
                   # input_length=self._history_length
    ))
    model.add(Dense(input_dim=self._hidden_neurons, output_dim=self._num_actions, activation='linear'))
    
    return model

