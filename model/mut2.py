#
# File: mut2.py
#
# Created: Tuesday, December  1 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#
try:
  from keras.models import Sequential
  from keras.layers.core import Dense
  from keras.layers.recurrent import JZS2
except ImportError:
  print('Keras NOT INSTALLED')
  
from .kerashistory import *

class MUT2Model(KerasHistory):
  """ Use MUT celss to associate value to sequence of observations
  """

  def __init__(self, num_actions, history_length, hidden_neurons):
    """
    
    Arguments:
    - `num_actions`:
    - `history_length`:
    - `hidden_neurons`:
    """
    super(MUT1Model, self).__init__(num_actions, history_length, hidden_neurons)
    
  def create_keras_model(self, state_size):
    model = Sequential()
    model.add(JZS2(input_shape=(self._history_length, state_size),
                   output_dim=self._hidden_neurons,
                   activation='tanh',
                   inner_activation='tanh'
    ))
    model.add(Dense(input_dim=self._hidden_neurons, output_dim=self._num_actions, activation='linear'))
    return model
