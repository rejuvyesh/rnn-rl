#
# File: kerashistory.py
#
# Created: Tuesday, December  1 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#

from .history import *

class KerasHistory(History):
  """ Base class for Keras based history
  """

  def __init__(self, num_actions, history_length, hidden_neurons):
    """ Constructor
    
    Arguments:
    - `num_actions`:
    - `history_length`:
    - `hidden_neurons`:
    """
    super(KerasHistory, self).__init__(num_actions, history_length)
    self._hidden_neurons = hidden_neurons
    

  def create_model(self, state_size):
    model = self.create_keras_model(state_size)
    
    print('Compiling Model')
    model.compile(loss='mse', optimizer='rmsprop')
    print('Compiled')
    
    return model
  
  def get_values(self, observations):
    print('------------------')
    print('ob: {0}'.format(observations.shape))
    
    return self._model.predict(X=observations, batch_size=len(observations))[0]
  
  def train_model(self, data, values):
    self._model.fit(X=data, y=values, batch_size=10, nb_epoch=4)
    
