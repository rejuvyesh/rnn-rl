#
# File: discrete.py
#
# Created: Monday, November 30 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#

from .abstract import *

class DiscreteModel(Model):
  """ Discrete States, no functional approximation
  """

  def __init__(self, num_actions):
    """
    
    Arguments:
    - `num_actions`:
    """
    super(DiscreteModel, self).__init__(num_actions)
    self._data = {}
    
  def values(self, episode):
    state = episode.states[-1]
    
    return [self._data.get(tuple(state+(action,)), 0.0) for action in range(self._num_actions)]
  
  def learn(self, episodes):
    for episode in episodes:
      for state, action, values in zip(episode.states, episode.actions, episode.values):
        self._data[tuple(state+(action,))] = values[action]
    

