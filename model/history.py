#
# File: history.py
#
# Created: Tuesday, December  1 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#

import numpy as np

from .abstract import Model

class History(Model):
  """ Tracking history
  """

  def __init__(self, num_actions, history_length):
    """ Constructor

    Arguments
    - `num_actions`:
    - `history_length`:
    """
    super(History, self).__init__(num_actions)
    
    self._history_length = history_length
    self._model = None
    
  def values(self, episode):
    if self._model is None:
      vals = [0.0] * self._num_actions
    else:
      num_st = min(len(episode.states), self._history_length)
      observations = self.make_data([list(episode.states)[-num_st:]])
      vals = self.get_values(observations)
      
    return vals
  
  def learn(self, episodes):
    state_size = len(episodes[0].states[0])
    
    if self._model is None:
      self._model = self.create_model(state_size)
      
    total_length = sum([len(ep.states) for ep in episodes])
    
    data = np.zeros(shape=(total_length, self._history_length, state_size))
    vals = []
    i = 0
    
    for e,ep in enumerate(episodes):
      states = list(ep.states)
      
      for t in range(len(states)):
        length = min(t+1, self._history_length)
        
        data[i, 0:length, :] = states[t+1-length:t+1]
        vals.append(ep.values[t])
        
        i += 1
        
    # Train
    print('Training Model')

    print(data.shape)
    self.train_model(data, self.make_data(vals))
    print('Done')
    
  def make_data(self, data):
    return np.array(data)
  
  def create_model(self, state_size):
    raise NotImplementedError()
  
  def get_values(self, observations):
    raise NotImplementedError()
  
  def train_model(self, data):
    raise NotImplementedError()
      

