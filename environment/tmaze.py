#
# File: tmaze.py
#
# Created: Wednesday, December  2 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#
import random

from .abstract import *
from .episode import *

class TMaze(Environment):
  """ Long corridor followed by T junction. Agent knows which direction to take initiall
but the information is lost. Therefore, partially observable
  """
  UP = 0
  DOWN = 1
  LEFT = 2
  RIGHT = 3

  def __init__(self, length, info_time):
    """ Create a new grid
    
    Arguments:
    - `length`:
    - `info_time`:
    """
    super(TMaze, self).__init__()
    self._length = length
    self._info_time = info_time
    self.reset()

  def num_actions(self):
    return 4

  def reset(self):
    self._current_pos = 0
    self._timestep = 0
    self._target_dir = random.choice([self.UP, self.DOWN])
    self._initial = self.make_state()
    
  def act(self, action):
    self._timestep += 1

    pos = (self._current_pos, 0)

    if action == self.UP:
      pos = (pos[0], -1)
    elif action == self.DOWN:
      pos = (pos[0], 1)
    elif action == self.LEFT:
      pos = (pos[0]-1, 0)
    elif action == self.RIGHT:
      pos = (pos[0]+1, 0)
    else:
      print('Huh?')

    if pso[0] == self._length-1 and pos[1] == -1:
      self._current_pos = pos[0]
      return (self.make_state(), 10.0 if self._target_dir == self.UP else 0.0, True)
    elif pos[0] == self.length-1 and pos[1] == 1:
      self._current_pos = pos[0]
      return (self.make_state(), 10.0 if self._target_dir == self.DOWN else 0.0, True)
    elif pos[1] == -1 or pos[1] == 1 or pos[0] < 0 or pos[0] >= self._length:
      return (self.make_state(), -2.0, False)
    else:
      self._current_pos = pos[0]
      return (self.make_state(), -1.0, False)

  def make_state(self):
    if self._timestep <= self._info_time:
      return (self._current_pos, self._target_dir+1)
    else:
      return (self._current_pos, 0)

