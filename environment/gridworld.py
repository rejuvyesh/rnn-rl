#
# File: gridworld.py
#
# Created: Monday, November 30 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#

from .abstract import *

class GridWorld(Environment):
  """ 2D GridWorld of given dimension
  """
  UP    = 0
  RIGHT = 1
  DOWN  = 2
  LEFT  = 3

  def __init__(self, width, height, initial, goal, obstacle, stochastic):
    """ Create a new GridWorld
    
    Arguments:
    - `width`:
    - `height`:
    - `initial`:
    - `goal`:
    - `obstacle`:
    - `stochastic`:
    """
    super(GridWorld, self).__init__()
    
    self._width = width
    self._height = height
    self._initial = initial
    self._goal = goal
    self._obstacle = obstacle
    self._stochastic = stochastic
    
    self.reset()

  def num_actions(self):
    return 4

  def reset(self):
    self._current_pos = self._initial
    if self._stochastic:
      self._initial = (random.randrange(self._width), random.randrange(self._height))
      
  def act(self, action):
    pos = self._current_pos
    
    if action == self.UP:
      pos = (pos[0], pos[1]-1) # Start from top heatmaps damn
    elif action == self.RIGHT:
      pos = (pos[0]+1, pos[1])
    elif action == self.DOWN:
      pos = (pos[0], pos[1]+1)
    elif action == self.LEFT:
      pos = (pos[0]-1, pos[1])
    else:
      print('Huh? No such action.')

    # Check if it makes sense now
    if pos == self._goal:
      return (pos, 10.0, True)
    elif pos[0] < 0 or pos[1] < 0 or pos[0] >= self._width or pos[1] >= self._height or pos == self._obstacle:
      return (self._current_pos, -5.0, False)
    else:
      self._current_pos = pos
      return (pos, -1.0, False)
    
