#
# File: abstract.py
#
# Created: Monday, November 30 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#
from __future__ import print_function, division, absolute_import

import random
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

from .episode import *

def identity_encode(state):
  """ Identity encoding, does not change state
  
  Arguments:
  - `state`:
  """
  return state


def _onehot_encode(state, ranges):
  """ Encode discrete variables to a sequence that each have a value of 0 or 1.
  
  Arguments:
  - `state`:
  - `ranges`:
  """
  result = [0.0] * sum(ranges)
  offset = 0
  
  for idx, val in enumerate(state):
    result[offset + int(val+0.49)] = 1.0
    offset += ranges[idx]
    
  return tuple(result)


def make_onehot_encode(ranges):
  """ Return an encode function
  
  Arguments:
  - `ranges`:
  """
  return lambda s: _onehot_encode(s, ranges)


class Environment(object):
  """ Abstract environment that produces new states
  """

  def __init__(self):
    """
    """
    self.encoding = identity_encode
    self._max_state = [-1e20] * 1000
    self._min_state = [1e20] * 1000
    
  def num_actions(self):
    raise NotImplementedError()
  
  def reset(self):
    raise NotImplementedError()
  
  def act(self, action):
    raise NotImplementedError()
  
  def act_supervised(self, action, target):
    self.act(action)
    
  def plot_model(self, model, name=None):
    episode = Episode()
    print('Plotting')

    if self._min_state == self._max_state or len(self._min_state) == 1:
      # 1D World
      x = []
      v = [[] for i in self.num_actions()]
      
      for i in np.arange(self._min_state[0], self._max_state, (self._max_state - self._min_state)/1000.0):
        x.append(i)
        
        episode.states.clear()
        episode.add_state(self.encoding((i,)))
        
        values = model.plot_values(episode)
        
        for act, val in enumerate(values):
          v[act].append(val)
          
      for a in range(self.num_actions()):
        plt.figure()
        plt.plot(x, v[a])
        if name is None:
          name='model'
        plt.savefig('{0}_{1:d}.pdf'.format(name, int(a))) # Each action
        
    elif len(self._min_state) == 2:
      # 2D world
      x_min = self._min_state[0]
      y_min = self._min_state[1]
      x_max = self._max_state[0]
      y_max = self._max_state[1]
      
      v = [np.ndarray(shape=(50,50)) for i in range(self.num_actions())]
      py = 0
      for y in np.arange(y_min, y_max, (y_max-y_min)/1000.0):
        px = 0
        for x in np.arange(x_min, x_min, (x_max-x_min)/1000.0):
          episode.state.clear()
          episode.add_state(self.encoding((x,y)))
          
          values = model.plot_values(episode)
          
          for act, val in enumerate(values):
            v[act][px,py] = val
          px+=1; py+=1
          
        for a in range(self.num_actions()):
          plt.figure()
          sns.heatmap(v[a], annot=False, fmt='d')
          if name is None:
            name = 'model'
          plt.savefig('{0}_{1:d}.pdf'.format(name, int(a)))
          
    else:
        print('Not Implemented')
        
        
  def run(self, model, learning, num_eps, max_eps_length, batch_size, verbose=True, start_eps=None):
    """ Run 

    Arguments:
    `model`:
    `learning`:
    `num_eps`:
    `max_eps_length`:
    `batch_size`:
    `verbose`:
    `start_eps`:
    """
    eps = []
    learn_eps = []
    
    possible_actions = list(range(self.num_actions()))
    
    try:
      for e in range(num_eps):
        # Initial State
        if start_eps is None:
          episode = Episode()
          episode.add_state(self.encoding(self._initial))
          self.reset()
        else:
          episode = copy.deepcopy(start_eps)
          
          self._initial = episode.states[0]
          self.reset()
          
          for action, target in zip(episode.actions, episode.states[0]):
            self.act_supervised(action, target)
            
        episode.add_value(model.values(episode))
        
        finished = False
        steps = 0
        while not finished:
          probas, _, _ = learning.actions(episode)
          if sum(probas) != 1:
            probas = np.array(probas)/sum(probas)
          action = np.random.choice(possible_actions, p=probas)
          state, reward, finished = self.act(action)
          
          self._min_state = [min(a,b) for a,b in zip(self._min_state, state)]
          self._max_state = [max(a,b) for a,b in zip(self._max_state, state)]
          
          episode.add(self.encoding(state), model.values(episode), action, reward)
          if steps >= max_eps_length:
            finished = True
          steps += 1
          
        learning.end_episode(episode)
        
        eps.append(episode)
        learn_eps.append(episode)
        
        if verbose:
          print(e, episode.cum_rewards)
          
        if len(learn_eps) == batch_size:
          model.learn(learn_eps)
          
          # Save memory
          for le in learn_eps:
            le.states = None
            le.values = None
            le.rewards = None
            le.actions = None

          learn_eps = []

    except KeyboardInterrupt:
      if not verbose:
        raise
      
    return eps
      
