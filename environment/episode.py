#
# File: episode.py
#
# Created: Monday, November 30 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#

import collections

MAX_EPISODE_LENGTH = 100

class Episode(object):
  """ Sequence of actions and observations
  """

  def __init__(self):
    self.states  = collections.deque([], MAX_EPISODE_LENGTH)
    self.values  = collections.deque([], MAX_EPISODE_LENGTH)
    self.actions = collections.deque([], MAX_EPISODE_LENGTH)
    self.rewards = collections.deque([], MAX_EPISODE_LENGTH)
    self.cum_rewards = 0.0

  def add_state(self, state):
    self.states.append(state)
    
  def add_value(self, value):
    self.values.append(value)

  def add_action(self, action):
    self.actions.append(action)
    
  def add_reward(self, reward):
    self.rewards.append(reward)
    self.cum_rewards += reward
    
  def add(self, state, value, action, reward):
    self.add_state(state)
    self.add_value(value)
    self.add_action(action)
    self.add_reward(reward)

  def __str__(self):
    return " states: {0} \n values: {1} \n actions: {2} \n rewards {3} \n cumulative: {4}".format(self.states, self.values, self.actions, self.rewards, self.cum_rewards)
