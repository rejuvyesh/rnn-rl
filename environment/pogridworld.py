#
# File: pogridworld.py
#
# Created: Wednesday, December  2 2015 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#
import random

from .gridworld import *

class PolarGridWorld(GridWorld):
  """ Grid of given dimensions. Agent can only observe its direction and distance from the wall in front of it.
  """

  TURN_LEFT = 0
  TURN_RIGHT = 1
  GO_FORWARD = 2
  GO_BACKWARD = 3

  def __init__(self, width, height, initial, goal, obstacle, stochastic):
    """ Create
    
    Arguments:
    - `width`:
    - `height`:
    - `initial`:
    - `goal`:
    - `obstacle`:
    - `stochastic`:
    """
    super(PolarGridWorld, self).__init__(width, height, initial, goal, obstacle, stochastic)

  def reset(self):
    super(PolarGridWorld, self).reset()
    self._current_dir = self.RIGHT
    
  def act(self, action):
    pos = self._current_dir
    
    if action == self.TURN_LEFT:
      self._current_dir = (self._current_dir + 1) % 4
    elif action == self.TURN_RIGHT:
      self._current_dir = (self._current_dir - 1) % 4
    else:
      if action == self.GO_FORWARD:
        offset = 1
      else:
        offset = -1
      if self._current_dir == self.UP:
        pos = (pos[0], pos[1]-offset)
      elif self._current_dir == self.DOWN:
        pos = (pos[0], pos[1]+offset)
      elif self._current_dir = self.LEFT:
        pos = (pos[0]-offset, pos[1])
      elif self._current_dir == self.RIGHT:
        pos = (pos[0]+offset, pos[1])
        
    finished = False
    
    if pos[0] < 0 or pos[1] < 0 or pos[0] >= self._width or pos[1] >= self._height or pos == self._obstacle:
      reward = -2.0
    else:
      self._current_pos = pos
      finished = (pos == self._goal)
      reward = 10.0 if finished else -1.0
      
    if self._current_dir == self.UP:
      dist = pos[1]
    elif self._current_dir == self.DOWN:
      dist = self._height - pos[1] - 1
    elif self._current_dir == self.LEFT:
      dist = pos[0]
    elif self._current_dir == self.RIGHT:
      dist = self._width - pos[0] - 1
      
    return ((dist, self._current_dir), reward, finished)
